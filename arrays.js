let arr = [3, 5, 2, 1, 4, 5, 9, 12];


// forEach -> не возвращает значений, работает как for
// filter -> возвращает массив [], callback должен вернуть boolean
// find -> возвращает 1 элемент (первое вхождение), callback должен вернуть boolean
// some, any -> возвращает true если в массиве присутствует хоть одно вхождение (совпадение)
//         callback должен вернуть boolean
// every -> возвращает true если в массиве все элементы соответствуют условию
//         callback должен вернуть boolean
// map -> возвращает новый массив callback должен вернуть что-нибудь
// reduce (reduceRight) -> (accum, item, index, self) -> возвращает содержимое accum


const numberFive = arr.find(element => element > 5);
if (numberFive) {
    const index = arr.indexOf(numberFive);
    arr.splice(index, 1, 0);
}

const isHaveZero = arr.every(item => item >= 0);

const newArray = arr.map(item => {
        if (item < 5) {
            return item + 10;
        }
        return item;
    }
);

console.log(newArray);

console.log(arr);
const result = arr.reduce((accum, item) => accum += item, 0);

console.log(result);

// .sort -> (current, next) влияет на исходный массив, callback должен возвращать -1, 0, 1



// создать массив чисел от 1 до 10 в случайном порядке
// с помощью одного из изученных методов перебора
// получить массив строк с названиями этих чисел
// [ 1, 3, 4] => ['один', 'три', 'четыре']
// * если для числа нет словесного описания подставлять 'hello'
// [2, 1000] => ['два', 'Hello'];

arr.sort((a, b) => b - a);

console.log(arr);

const text = ['Hello', 'good bye', 'Warning', 'test', 'first', 'apple', 'apple' , 'apply'];

function sortString(a, b) {
    // const a_1 = a.toLowerCase();
    // const b_1 = b.toLowerCase();
    return a.localeCompare(b);
}

text.sort(sortString);

console.log(text);
