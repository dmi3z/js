let arr = [3, 5, 2, 1, 4, 5, 9, 12];

const maxNumber = Math.min(...arr);

console.log(maxNumber);

//-----

// .round - округляет до целого по правилам математики
// .floor - округляет до целого в меньшую сторону 2,9999 -> 2
// .ceil - округляет до целого в большую сторону 2,1 -> 3
// .random - генерирует псевдо случайное число от 0 до 1

const random = Math.random();

console.log(random);

function getRandom(min, max) {
    let random = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(random);
}

console.log(getRandom(10, 100));


function getNumberName(number) {
    switch (number) {
        case 1:
            return 'Один';

        case 2:
            return 'Два';

        case 3:
            return 'Три';

        case 4:
            return 'Четыре';

        case 5:
            return 'Пять';

        default:
            return 'Hello';
    }
}

// const numberNames = [];

/* for (let i = 0; i < arr.length; i++) {
    // const numberName = getNumberName(arr[i]);
    numberNames.push(getNumberName(arr[i]));
} */

// arr.forEach(item => numberNames.push(getNumberName(item))); // (item, index, self)

// const numberNames = arr.reduce((accum, item) => accum.concat(getNumberName(item)), []); // (accum, item, index, self)

const numberNames = arr.map(item => getNumberName(item));

console.log(numberNames);

// setTimeout - выполняется единожды, setInterval - выполняется постоянно с задданным интервалом

const timer = setInterval(sayHello, 2000);
// clearInterval(timer);

function sayHello() {
    console.log('Hello');
}

/*
вывести на консоль 4 раза слово 'Hello' c периодичностью 1 сек
после чего остановить выполнение 
*/
