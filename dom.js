

/*
1. Записать в переменную ссылку на тег (по id)
2. Обратится к полученной переменной (к содержимому тега)
и установить нужное значение


1 ----
document
    .getElementsByTagName('название_тега') => []
    .getElementsByClassName('название_класса') => []
    .getElementById('название_id') => ссылка на элемент

    .querySelector('.название_класса, #название_id', 'название тега') => ссылка на элемент
    .querySelectorAll('.название_класса, 'название тега') => []

2 -----
    let myTag
        .innerText = 'Hello';
        .innerHTML = '<div>Hello</div>';
*/
const timeTag = document.querySelector('#time');

setInterval(() => {
    const currentTime = moment().format('HH:mm:ss');
    timeTag.innerText = currentTime;
}, 1000);

// 3. Для изменения стилей:
// timeTag
//     .style
//          .color = 'red';
//          .marginTop = '100px';

timeTag.style.color = 'red';
timeTag.style.marginTop = '100px';

// 4. Для управления классами
/* timeTag <h1 class="my-class"></h1>
    .className = 'название_класса1 название_класса2'; <h1 class="название_класса"></h1>
    .classList
        .add('название_класса') -> дописывает новый класс к существующим (если такой класс уже
            есть то ничего не произойдет)
        .remove('название_класса') -> если такой класс найден, то удаляет, если нет
            то ничего не произойдет
*/

// timeTag.className = 'italic';

console.log(timeTag);
timeTag.classList.add('italic');

// ----------
const list = document.getElementsByClassName('item');
console.log(list)

for (let i = 0; i < list.length; i++) {
    // list[i].classList.remove('item');
    if (i % 2 === 0) {
        // list[i].style.backgroundColor = 'gray';
        list[i].classList.add('even');
        console.log(list[i].parentNode);
    } else {
        // list[i].style.color = 'blue';
        list[i].classList.add('odd');
    }
}